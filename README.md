# temperantia wallpapers

**Appx. reading time: 2 minutes**  

The colors of the temperantia wallpapers are based on the temperantia palette and are fairly subdued. Overall, they are meant to provide a background that helps with clarity and separation of windows, while at the same time being somewhat colorful and stimulating. They come in light and dark variants, and two different color versions for each of these.  

Perhaps most importantly, using any of these wallpapers will make the desktop's corners **rounded**, which can be seen in the previews below. The rounded corners aren't *purely* there for decorative purposes, though. The main reason, actually, is to do with how the temperantia GTK theme styles maximized and tiled windows - which almost always stay rounded. Because of this, a wallpaper image with square corners can sometimes be quite visible in a screen's bottom corners when windows are maximized or tiled.  

If you don't mind the issue described above regarding using a wallpaper with squared corners, you can of course use any wallpaper you like together with the temperantia GTK theme. And if you *do* mind it, but want to use another wallpaper, the temperantia wallpapers come with a couple of templates to create rounded corners on your own wallpaper images - see below.  

## Previews

![Screenshot of temperantia wallpaper, light variant, with a pink and blue-green colored sphere in the center](images/temperantia-wallpaper-I-light.png)  

![Screenshot of temperantia wallpaper, dark variant, with a pink and blue-green colored sphere in the center](images/temperantia-wallpaper-I-dark.png)  

# How to use the templates

The rounded corners of the temperantia wallpapers are accomplished very simply. The wallpaper image isn't itself rounded, because if you only round that off, there will be some transparent space in each corner. To have as much control as possible and make the corners blend well with the bezels of a display, these transparent corner spaces need to be black.  

So, on top of a temperantia wallpaper image, there's another image which consists *only* of these spaces, with an inner radius of 8px. But instead of being transparent, they are colored black. Effectively, this rounds off the corners of the wallpaper. The templates, then, consist solely of these, so to speak, empty images with small, black corners.  

All of the templates are located in the directory `temperantia-wallpapers/templates`, where you'll find templates for a few different screen resolutions and aspect ratios:  

* 3:2 2160x1440  

* 3:2 2560x1700  

* 16:9 1920x1080  

* 16:9 2560x1440  

* 16:10 2560x1600  

The templates are all in the SVG file format, and in the guide below it's assumed you have [Inkscape](https://inkscape.org/ "https://inkscape.org/") installed or is willing to install it.  

**1.** Begin by opening the template file you need in Inkscape.  

**2.** Import you wallpaper image by dragging and dropping it into Inkscape, **or** by pressing <kbd>CTRL</kbd> + <kbd>I</kbd>, and then choosing your image in the file chooser dialog.  

**3.** Align your imported image with the template image by, firstly, selecting your image, then putting `0` in the **X** and **Y** entries in the top toolbar.  

**4.** Your image also has to be placed underneath the template image. So, with your image still selected, simply press the <kbd>Page Down</kbd> key. You can **also** do this by going to "Object" in the menubar, choosing "Objects" in the menu, and in the dialog window that opens - with your image being selected - pressing the **down arrow** located in the dialog's bottom half.  

**5.** Now check that your image has been rounded by zooming in on one of its corners. If so, press <kbd>SHIFT</kbd> + <kbd>CTRL</kbd> + <kbd>S</kbd> to save your rounded wallpaper image as a new file. (You can also press <kbd>SHIFT</kbd> + <kbd>CTRL</kbd> + <kbd>E</kbd> to export the SVG file to PNG.)  
